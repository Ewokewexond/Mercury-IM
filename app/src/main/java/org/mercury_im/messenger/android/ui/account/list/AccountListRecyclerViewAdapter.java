package org.mercury_im.messenger.android.ui.account.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.account.OnAccountListItemClickListener;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountViewItem;
import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.android.ui.avatar.AvatarDrawable;
import org.mercury_im.messenger.android.util.AbstractDiffCallback;

import java.util.ArrayList;
import java.util.List;

public class AccountListRecyclerViewAdapter extends RecyclerView.Adapter<AccountListRecyclerViewAdapter.ViewHolder> {

    private final List<AccountViewItem> connectionStates = new ArrayList<>();
    private final OnAccountListItemClickListener onAccountClickListener;
    private final AndroidAccountListViewModel viewModel;

    public AccountListRecyclerViewAdapter(AndroidAccountListViewModel viewModel, OnAccountListItemClickListener listener) {
        onAccountClickListener = listener;
        this.viewModel = viewModel;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_account, parent, false);

        return new ViewHolder(view);
    }

    public void setValues(List<AccountViewItem> values) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(
                new AccountsDiffCallback(values, connectionStates), true);
        connectionStates.clear();
        connectionStates.addAll(values);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return connectionStates.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        AccountViewItem viewItem = connectionStates.get(position);
        Account account = viewItem.getAccount();

        holder.jid.setText(account.getAddress());
        holder.avatar.setImageDrawable(new AvatarDrawable(account.getAddress(), account.getAddress()));
        holder.enabled.setChecked(account.isEnabled());
        holder.enabled.setOnCheckedChangeListener((compoundButton, checked) -> {
            if (!compoundButton.isPressed()) {
                return;
            }
            viewModel.setAccountEnabled(account, checked);
        });
        holder.status.setText(viewItem.getConnectivityState().toString());

        holder.mView.setOnLongClickListener(v -> {
            onAccountClickListener.onAccountListItemLongClick(account);
            return true;
        });

        holder.mView.setOnClickListener(v -> onAccountClickListener.onAccountListItemClick(account));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView avatar;
        final TextView jid;
        final Switch enabled;
        final TextView status;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            avatar = view.findViewById(R.id.avatar_account);
            jid = view.findViewById(R.id.text_account_jid);
            enabled = view.findViewById(R.id.switch_account_enabled);
            status = view.findViewById(R.id.text_account_status);
        }
    }

    public class AccountsDiffCallback extends AbstractDiffCallback<AccountViewItem> {

        public AccountsDiffCallback(List<AccountViewItem> newItems, List<AccountViewItem> oldItems) {
            super(newItems, oldItems);
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItems.get(oldItemPosition).getAccount().getId()
                    .equals(newItems.get(newItemPosition).getAccount().getId());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            AccountViewItem oldM = oldItems.get(oldItemPosition);
            AccountViewItem newM = newItems.get(newItemPosition);
            return oldM.equals(newM);
        }
    }
}
