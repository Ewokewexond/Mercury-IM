package org.mercury_im.messenger.android.ui.account.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.ui.openpgp.ToggleableFingerprintsAdapter;
import org.mercury_im.messenger.android.ui.openpgp.OpenPgpV4FingerprintFormatter;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountDetailsFragment extends Fragment {

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @BindView(R.id.jid)
    TextView jid;

    @BindView(R.id.btn_share)
    Button localFingerprintShareButton;

    @BindView(R.id.local_fingerprint)
    TextView localFingerprint;

    @BindView(R.id.fingerprint_list)
    RecyclerView externalFingerprintRecyclerView;

    @BindView(R.id.other_fingerprints_card)
    MaterialCardView otherFingerprintsLayout;

    private final UUID accountId;
    private ToggleableFingerprintsAdapter adapter;

    private AndroidAccountDetailsViewModel viewModel;

    public AccountDetailsFragment(UUID accountId) {
        this.accountId = accountId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_details, container, false);
        ButterKnife.bind(this, view);

        externalFingerprintRecyclerView.setAdapter(adapter);

        observe();

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidAccountDetailsViewModel.AndroidAccountDetailsViewModelFactory factory =
                new AndroidAccountDetailsViewModel.AndroidAccountDetailsViewModelFactory(MercuryImApplication.getApplication(), accountId);
        viewModel = new ViewModelProvider(this, factory)
                .get(AndroidAccountDetailsViewModel.class);

        this.adapter = new ToggleableFingerprintsAdapter(
                (fingerprint, checked) -> viewModel.markFingerprintTrusted(fingerprint, checked));
        this.adapter.setItemLongClickListener(fingerprint -> viewModel.unpublishPublicKey(fingerprint));
    }

    private void observe() {
        viewModel.getLocalFingerprint().observe(getViewLifecycleOwner(),
                f -> localFingerprint.setText(OpenPgpV4FingerprintFormatter.formatOpenPgpV4Fingerprint(f)));

        viewModel.getRemoteFingerprints().observe(getViewLifecycleOwner(), items -> {
            otherFingerprintsLayout.setVisibility(items.isEmpty() ? View.GONE : View.VISIBLE);
            adapter.setItems(items);
        });

        viewModel.getJid().observe(getViewLifecycleOwner(), accountJid -> jid.setText(accountJid.toString()));

        localFingerprintShareButton.setOnClickListener(v -> {
            OpenPgpV4Fingerprint fingerprint = viewModel.getLocalFingerprint().getValue();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "openpgp4fpr:" + fingerprint);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, "Share OpenPGP Fingerprint");
            startActivity(shareIntent);

        });
    }

}
