package org.mercury_im.messenger.android.ui.roster.contacts;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.entity.contact.Peer;
import org.mercury_im.messenger.android.ui.avatar.AvatarDrawable;
import org.mercury_im.messenger.android.ui.roster.contacts.detail.ContactDetailActivity;
import org.mercury_im.messenger.android.ui.util.AbstractRecyclerViewAdapter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactListRecyclerViewAdapter
        extends AbstractRecyclerViewAdapter<Peer, ContactListRecyclerViewAdapter.RosterItemViewHolder> {

    public ContactListRecyclerViewAdapter() {
        super(new ContactDiffCallback());
    }

    @NonNull
    @Override
    public RosterItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RosterItemViewHolder(parent.getContext(), LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RosterItemViewHolder holder, int position) {
        Peer peer = getItemAt(position);
        holder.bind(peer);
    }

    public class RosterItemViewHolder extends RecyclerView.ViewHolder {

        private View view;

        @BindView(R.id.roster_entry__jid)
        TextView jidView;

        @BindView(R.id.roster_entry__name)
        TextView nameView;

        @BindView(R.id.roster_entry__avatar)
        ImageView avatarView;

        Context context;

        public RosterItemViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            this.view = itemView;
            ButterKnife.bind(this, view);
        }

        void bind(Peer contact) {
            String name = contact.getDisplayName();
            String address = contact.getAddress();
            nameView.setText(name);
            jidView.setText(address);
            avatarView.setImageDrawable(new AvatarDrawable(name, address));
            view.setOnClickListener(view -> {

                Intent intent = new Intent(context, ContactDetailActivity.class);
                intent.putExtra(ContactDetailActivity.EXTRA_JID, address);
                intent.putExtra(ContactDetailActivity.EXTRA_ACCOUNT, contact.getAccount().getId().toString());

                context.startActivity(intent);
            });
        }
    }

    private static class ContactDiffCallback extends AbstractDiffCallback<Peer> {

        ContactDiffCallback() {
            super(true);
        }

        @Override
        public boolean areItemsTheSame(Peer oldItem, Peer newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(Peer oldItem, Peer newItem) {
            return areItemsTheSame(oldItem, newItem) &&
                    Objects.equals(oldItem.getName(), newItem.getName());
        }
    }
}
