package org.mercury_im.messenger.android.di.module;

import android.app.Application;

import org.mercury_im.messenger.BuildConfig;
import org.mercury_im.messenger.data.model.Models;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

@Module
public class AndroidDatabaseModule {

    @Provides
    @Singleton
    static ReactiveEntityStore<Persistable> provideDatabase(Application application) {
        // override onUpgrade to handle migrating to a new version
        DatabaseSource source = new DatabaseSource(application, Models.DEFAULT, "mercury_req_db", 1);
        if (BuildConfig.DEBUG) {
            source.setTableCreationMode(TableCreationMode.DROP_CREATE);
        }
        Configuration configuration = source.getConfiguration();
        return ReactiveSupport.toReactiveStore(new EntityDataStore<>(configuration));
    }
}
