package org.mercury_im.messenger.android.ui.openpgp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.core.viewmodel.openpgp.FingerprintViewItem;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ToggleableFingerprintsAdapter extends RecyclerView.Adapter<ToggleableFingerprintsAdapter.ViewHolder> {

    private final List<FingerprintViewItem> fingerprints = new ArrayList<>();
    private final OnFingerprintItemToggleListener toggleListener;
    private OnFingerprintItemLongClickListener longClickListener = null;

    private static final DateFormat dateFormat = SimpleDateFormat.getDateInstance();

    public ToggleableFingerprintsAdapter(OnFingerprintItemToggleListener toggleListener) {
        this.toggleListener = toggleListener;
    }

    public void setItems(List<FingerprintViewItem> fingerprints) {
        synchronized (this.fingerprints) {
            this.fingerprints.clear();
            this.fingerprints.addAll(fingerprints);
            this.notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_fingerprint_toggleable, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        synchronized (fingerprints) {
            FingerprintViewItem f = fingerprints.get(position);

            final OpenPgpV4Fingerprint fingerprint = f.getFingerprint();

            holder.fingerprint.setText(OpenPgpV4FingerprintFormatter.formatOpenPgpV4Fingerprint(fingerprint));
            holder.fingerprintTimestamp.setText(dateFormat.format(f.getModificationDate()));

            holder.trustSwitch.setChecked(f.isTrusted());
            holder.trustSwitch.setOnCheckedChangeListener(
                    (buttonView, isChecked) -> toggleListener.onFingerprintToggled(fingerprint, isChecked));
            holder.divider.setVisibility(position == fingerprints.size() - 1 ? View.GONE : View.VISIBLE);

            holder.itemView.setOnLongClickListener(v -> {
                if (longClickListener != null) {
                    longClickListener.onFingerprintItemLongClick(fingerprint);
                }
                return true;
            });
        }
    }

    @Override
    public int getItemCount() {
        return fingerprints.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final Switch trustSwitch;
        private final TextView fingerprintTimestamp;
        private final TextView fingerprint;
        private final View divider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.fingerprint = itemView.findViewById(R.id.fingerprint);
            this.trustSwitch = itemView.findViewById(R.id.fingerprint_toggle);
            this.fingerprintTimestamp = itemView.findViewById(R.id.fingerprint_timestamp);
            this.divider = itemView.findViewById(R.id.divider);
        }
    }

    public void setItemLongClickListener(OnFingerprintItemLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }

    public interface OnFingerprintItemToggleListener {

        void onFingerprintToggled(OpenPgpV4Fingerprint fingerprint, boolean checked);
    }

    public interface OnFingerprintItemLongClickListener {

        void onFingerprintItemLongClick(OpenPgpV4Fingerprint fingerprint);
    }
}
