package org.mercury_im.messenger.android.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import org.jivesoftware.smackx.ping.android.ServerPingWithAlarmManager;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.Notifications;
import org.mercury_im.messenger.android.ui.MainActivity;

/**
 * Started, Bound Service, which is responsible keeping the application alive when the app is not open.
 */
public class MercuryForegroundService extends Service {

    private static final String APP = "org.mercury-im.messenger";
    private static final String SERVICE = APP + ".MercuryForegroundService";

    private static final String ACTION = SERVICE + ".ACTION";

    // ACTIONS
    public static final String ACTION_START = ACTION + ".START";
    public static final String ACTION_STOP = ACTION + ".STOP";


    @NonNull
    @Override
    public final IBinder onBind(Intent intent) {
        return new Binder(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MercuryImApplication.getApplication().getAppComponent().inject(this);
        beginLifecycleOfPingManager();
    }

    /**
     * PingManager will ensure the XMPP connection is kept alive.
     */
    private void beginLifecycleOfPingManager() {
        ServerPingWithAlarmManager.onCreate(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        endLifecycleOfPingManager();
    }

    private void endLifecycleOfPingManager() {
        ServerPingWithAlarmManager.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            startAndDisplayForegroundNotification();
        } else {
            String action = intent.getAction();
            action = action == null ? "" : action;

            switch (action) {
                case ACTION_START:
                    startAndDisplayForegroundNotification();
                    break;
                case ACTION_STOP:
                    stopForeground(true);
                    stopSelf();
                    break;
                default:
                    break;
            }
        }

        return START_STICKY_COMPATIBILITY;
    }

    private void startAndDisplayForegroundNotification() {
        Notification notification = buildForegroundNotification(getApplicationContext());
        startForeground(Notifications.FOREGROUND_SERVICE_ID, notification);
    }

    private static Notification buildForegroundNotification(Context context) {
        Intent startMainActivityIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                startMainActivityIntent, 0);
        return new NotificationCompat.Builder(context, Notifications.NOTIFICATION_CHANNEL__FOREGROUND_SERVICE)
                .setSmallIcon(R.drawable.ic_send_black_24dp)
                .setContentIntent(pendingIntent)
                .build();
    }

    public class Binder extends android.os.Binder {

        private final MercuryForegroundService service;

        public Binder(MercuryForegroundService service) {
            this.service = service;
        }

        public MercuryForegroundService getService() {
            return service;
        }
    }
}
