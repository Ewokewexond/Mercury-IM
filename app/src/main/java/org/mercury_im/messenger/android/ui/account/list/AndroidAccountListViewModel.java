package org.mercury_im.messenger.android.ui.account.list;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountViewItem;
import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.android.ui.MercuryAndroidViewModel;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountListViewModel;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

public class AndroidAccountListViewModel extends AndroidViewModel implements MercuryAndroidViewModel<AccountListViewModel> {

    private static final Logger LOGGER = Logger.getLogger(AndroidAccountListViewModel.class.getName());

    private final MutableLiveData<List<AccountViewItem>> accounts = new MutableLiveData<>(Collections.emptyList());

    @Inject
    AccountListViewModel viewModel;

    public AndroidAccountListViewModel(@NonNull Application application) {
        super(application);

        MercuryImApplication.getApplication().getAppComponent().inject(this);

        addDisposable(getCommonViewModel().observeAccounts()
                .subscribe(accounts::postValue, error -> LOGGER.log(Level.SEVERE, "Error observing accounts", error)));
    }

    public LiveData<List<AccountViewItem>> getAccounts() {
        return accounts;
    }

    public void setAccountEnabled(Account accountModel, boolean enabled) {
        getCommonViewModel().onToggleAccountEnabled(accountModel, enabled);
    }

    @Override
    public AccountListViewModel getCommonViewModel() {
        return viewModel;
    }

    public void onDeleteAccount(UUID accountId) {
        getCommonViewModel().onDeleteAccount(accountId);
    }
}
