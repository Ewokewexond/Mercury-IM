package org.mercury_im.messenger.android.ui.chat;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.ui.MercuryAndroidViewModel;
import org.mercury_im.messenger.core.Messenger;
import org.mercury_im.messenger.core.SchedulersFacade;
import org.mercury_im.messenger.core.data.repository.DirectChatRepository;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.core.viewmodel.chat.ChatViewModel;
import org.mercury_im.messenger.data.repository.RxMessageRepository;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.contact.Peer;
import org.mercury_im.messenger.entity.message.Message;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AndroidChatViewModel extends ViewModel implements MercuryAndroidViewModel<ChatViewModel> {

    @Inject
    ChatViewModel commonViewModel;

    @Inject
    SchedulersFacade schedulers;

    private MutableLiveData<Peer> contact = new MutableLiveData<>();
    private MutableLiveData<List<Message>> messages = new MutableLiveData<>();
    private MutableLiveData<String> contactDisplayName = new MutableLiveData<>();
    private MutableLiveData<DirectChat> chat = new MutableLiveData<>();

    public AndroidChatViewModel() {
        super();
        MercuryImApplication.getApplication().getAppComponent().inject(this);
    }

    public void init(UUID accountId, EntityBareJid jid) {
        commonViewModel.init(accountId, jid);

        addDisposable(commonViewModel.getPeer()
                .filter(Optional::isPresent).map(Optional::getItem)
                .compose(schedulers.executeUiSafeObservable())
                .subscribe(contact::setValue));

        addDisposable(commonViewModel.getMessages()
                .compose(schedulers.executeUiSafeObservable())
                .subscribe(messages::setValue));

        addDisposable(commonViewModel.getContactDisplayName()
                .compose(schedulers.executeUiSafeObservable())
                .subscribe(contactDisplayName::setValue));

        addDisposable(commonViewModel.getChat()
                .compose(schedulers.executeUiSafeObservable())
                .subscribe(chat::setValue));
    }

    public LiveData<List<Message>> getMessages() {
        return messages;
    }

    public LiveData<Peer> getContact() {
        return contact;
    }

    public LiveData<String> getContactDisplayName() {
        return contactDisplayName;
    }

    public void queryTextChanged(String query) {
        commonViewModel.onQueryTextChanged(query);
    }

    public void deleteContact() {
        getCommonViewModel().deleteContact();
    }

    public void sendMessage(String body) {
        getCommonViewModel().sendMessage(body);
    }

    @Override
    public ChatViewModel getCommonViewModel() {
        return commonViewModel;
    }
}
