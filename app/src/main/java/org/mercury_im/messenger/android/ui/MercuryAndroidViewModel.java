package org.mercury_im.messenger.android.ui;

import org.mercury_im.messenger.core.viewmodel.MercuryViewModel;

import io.reactivex.disposables.Disposable;

public interface MercuryAndroidViewModel<VM extends MercuryViewModel> {

    VM getCommonViewModel();

    default void dispose() {
        getCommonViewModel().dispose();
    }

    default void addDisposable(Disposable disposable) {
        getCommonViewModel().addDisposable(disposable);
    }
}
