package org.mercury_im.messenger.android;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.android.ui.chat.ChatActivity;

import java.util.UUID;

public class Notifications {

    public static final String NOTIFICATION_CHANNEL__FOREGROUND_SERVICE = "foreground_service";
    public static final String NOTIFICATION_CHANNEL__NEW_MESSAGE = "new_message";
    public static final String NOTIFICATION_CHANNEL__INCOMING_CALL = "incoming_call"; // One day...
    public static final String NOTIFICATION_CHANNEL__DEBUG = "debug";

    // Notification IDs
    public static final int FOREGROUND_SERVICE_ID = 1;

    public static void initializeNotificationChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            // No not call below code on lower API levels
            return;
        }
        createForegroundNotificationChannel(context);
        createMessageNotificationChannel(context);
    }

    @TargetApi(26)
    private static void createForegroundNotificationChannel(Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelName = context.getResources().getString(R.string.channel_name_foreground);
        String channelDescription = context.getResources().getString(R.string.channel_description_foreground);

        NotificationChannel channel = new NotificationChannel(
                NOTIFICATION_CHANNEL__FOREGROUND_SERVICE, channelName, NotificationManager.IMPORTANCE_MIN);
        channel.setDescription(channelDescription);
        channel.setShowBadge(false);
        notificationManager.createNotificationChannel(channel);
    }

    @TargetApi(26)
    private static void createMessageNotificationChannel(Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelName = context.getResources().getString(R.string.channel_name_message);
        String channelDescription = context.getResources().getString(R.string.channel_description_message);

        NotificationChannel channel = new NotificationChannel(
                NOTIFICATION_CHANNEL__NEW_MESSAGE, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(channelDescription);
        notificationManager.createNotificationChannel(channel);
    }

    public static int directChatMessageReceived(Context context, DirectChat chat, String contactName, String body) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        UUID id = chat.getId();

        Intent tapAction = new Intent(context, ChatActivity.class);
        tapAction.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        tapAction.putExtra(ChatActivity.EXTRA_JID, chat.getPeer().getAddress());
        tapAction.putExtra(ChatActivity.EXTRA_ACCOUNT, chat.getPeer().getAccount().getId());
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, tapAction, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                Notifications.NOTIFICATION_CHANNEL__NEW_MESSAGE);
        builder.setContentTitle(context.getResources().getString(R.string.notification_title, contactName));
        builder.setContentText(body);
        builder.setSmallIcon(R.drawable.ic_person_outline_black_24dp);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        notificationManagerCompat.notify(id.hashCode(), builder.build());

        return id.hashCode();
    }

}
