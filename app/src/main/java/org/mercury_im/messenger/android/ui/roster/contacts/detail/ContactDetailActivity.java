package org.mercury_im.messenger.android.ui.roster.contacts.detail;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.R;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailActivity extends AppCompatActivity {
    public static final String EXTRA_JID = "JID";
    public static final String EXTRA_ACCOUNT = "ACCOUNT";

    @BindView(R.id.fragment)
    FrameLayout container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        ButterKnife.bind(this);

        MercuryImApplication.getApplication().getAppComponent().inject(this);

        if (savedInstanceState == null) {
            savedInstanceState = getIntent().getExtras();
            if (savedInstanceState == null) return;
        }

        String jidString = savedInstanceState.getString(EXTRA_JID);
        if (jidString != null) {
            UUID accountId = UUID.fromString(savedInstanceState.getString(EXTRA_ACCOUNT));

            ContactDetailViewModel viewModel = new ViewModelProvider(this).get(ContactDetailViewModel.class);
            viewModel.bind(accountId, jidString);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new ContactDetailFragment(), "contact_details").commit();
    }
}
