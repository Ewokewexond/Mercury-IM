package org.mercury_im.messenger.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.account.detail.AccountDetailsFragment;
import org.mercury_im.messenger.android.ui.account.DeleteAccountDialogFragment;
import org.mercury_im.messenger.android.ui.account.OnAccountListItemClickListener;
import org.mercury_im.messenger.android.ui.account.list.AccountListFragment;
import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.android.ui.chatlist.ChatListFragment;
import org.mercury_im.messenger.android.ui.roster.RosterFragment;
import org.mercury_im.messenger.android.ui.settings.SettingsActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnAccountListItemClickListener, SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    private ChatListFragment chatListFragment = new ChatListFragment();
    private RosterFragment rosterFragment = new RosterFragment();
    private AccountListFragment accountListFragment = new AccountListFragment();
    private SearchView.OnQueryTextListener searchViewListeningFragment;

    @Inject
    AccountRepository accountRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MercuryImApplication.getApplication().getAppComponent().inject(this);

        setSupportActionBar(toolbar);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);
        bottomNavigationView.setSelectedItemId(R.id.entry_chats);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (menuItem.getItemId()) {

            case R.id.entry_chats:
                transaction.replace(R.id.fragment, chatListFragment).commit();
                searchViewListeningFragment = chatListFragment;
                return true;

            case R.id.entry_contacts:
                transaction.replace(R.id.fragment, rosterFragment).commit();
                searchViewListeningFragment = rosterFragment;
                return true;

            case R.id.entry_accounts:
                transaction.replace(R.id.fragment, accountListFragment).commit();
                searchViewListeningFragment = accountListFragment;
                return true;
        }
        return false;
    }

    @Override
    public void onAccountListItemClick(Account item) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new AccountDetailsFragment(item.getId())).commit();
    }

    @Override
    public void onAccountListItemLongClick(Account item) {
        new DeleteAccountDialogFragment(item.getId())
                .show(getSupportFragmentManager(), "DELETE");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return searchViewListeningFragment.onQueryTextSubmit(query);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return searchViewListeningFragment.onQueryTextChange(newText);
    }
}
