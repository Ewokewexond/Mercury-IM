package org.mercury_im.messenger.core.store.message;

import org.mercury_im.messenger.entity.Account;

public interface MercuryMessageStoreFactory {

    MercuryMessageStore createMessageStore(Account account);
}
