package org.mercury_im.messenger.core.data.repository;

import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.contact.Peer;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface DirectChatRepository {

    Single<DirectChat> insertDirectChat(DirectChat chat);

    default Observable<Optional<DirectChat>> observeDirectChat(DirectChat chat) {
        return observeDirectChat(chat.getId());
    }

    Observable<Optional<DirectChat>> observeDirectChat(UUID chatId);

    Maybe<DirectChat> getDirectChat(UUID chatId);

    Single<DirectChat> getOrCreateChatWithPeer(Peer peer);

    Observable<Optional<DirectChat>> observeDirectChatByPeer(Peer peer);

    Maybe<DirectChat> getDirectChatByPeer(Peer peer);

    Observable<List<DirectChat>> observeAllDirectChats();

    Single<DirectChat> updateDirectChat(DirectChat chat);

    Single<DirectChat> upsertDirectChat(DirectChat chat);

    default Completable deleteDirectChat(DirectChat chat) {
        return deleteDirectChat(chat.getId());
    }

    Completable deleteDirectChat(UUID chatId);

    Observable<List<DirectChat>> findChatsByQuery(String query);
}
