package org.mercury_im.messenger.core.viewmodel.openpgp;

public enum OxBackupRestoreError {

    invalid_backup_code,
    no_backup_found,
    invalid_user_id_on_key,
    pgp_error,
    not_authenticated_error,
    protocol_error,
    other_error
}
