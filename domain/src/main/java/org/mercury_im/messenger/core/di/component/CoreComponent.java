package org.mercury_im.messenger.core.di.component;

import org.mercury_im.messenger.core.viewmodel.account.LoginViewModel;
import org.mercury_im.messenger.core.viewmodel.account.detail.AccountDetailsViewModel;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountListViewModel;
import org.mercury_im.messenger.core.viewmodel.chat.ChatViewModel;
import org.mercury_im.messenger.core.viewmodel.openpgp.OxSecretKeyBackupRestoreViewModel;

import dagger.Component;

@Component
public interface CoreComponent {

    void inject(LoginViewModel viewModel);

    void inject(AccountListViewModel viewModel);

    void inject(AccountDetailsViewModel viewModel);

    void inject(ChatViewModel viewModel);

    void inject(OxSecretKeyBackupRestoreViewModel viewModel);
}
