package org.mercury_im.messenger.core;

import org.mercury_im.messenger.core.listener.IncomingDirectMessageListener;
import org.mercury_im.messenger.entity.chat.Chat;
import org.mercury_im.messenger.entity.message.Message;

import io.reactivex.Completable;

public interface MessageCenter<C extends Chat> {

    Messenger getMessenger();

    Completable sendMessage(Message message, C chat);

    void addIncomingMessageListener(IncomingDirectMessageListener listener);
}
