package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.Messenger;
import org.mercury_im.messenger.core.SchedulersFacade;
import org.mercury_im.messenger.core.connection.MercuryConnectionManager;
import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.core.data.repository.DirectChatRepository;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.core.data.repository.OpenPgpRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.core.viewmodel.account.LoginViewModel;
import org.mercury_im.messenger.core.viewmodel.account.detail.AccountDetailsViewModel;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountListViewModel;
import org.mercury_im.messenger.core.viewmodel.chat.ChatListViewModel;
import org.mercury_im.messenger.core.viewmodel.chat.ChatViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {

    @Provides
    @Singleton
    static LoginViewModel provideLoginViewModel(MercuryConnectionManager connectionManager,
                                                AccountRepository accountRepository,
                                                SchedulersFacade schedulers) {
        return new LoginViewModel(connectionManager, accountRepository, schedulers);
    }

    @Provides
    @Singleton
    static AccountListViewModel provideAccountsViewModel(MercuryConnectionManager connectionManager,
                                                         AccountRepository accountRepository,
                                                         OpenPgpRepository openPgpRepository,
                                                         SchedulersFacade schedulers) {
        return new AccountListViewModel(connectionManager, accountRepository, openPgpRepository, schedulers);
    }

    @Provides
    @Singleton
    static AccountDetailsViewModel provideAccountDetailsViewModel(MercuryConnectionManager connectionManager,
                                                                  OpenPgpRepository openPgpRepository,
                                                                  AccountRepository accountRepository,
                                                                  SchedulersFacade schedulers) {
        return new AccountDetailsViewModel(connectionManager, openPgpRepository, accountRepository, schedulers);
    }

    @Provides
    @Singleton
    static ChatViewModel provideChatViewModel(Messenger messenger,
                                              PeerRepository peerRepository,
                                              DirectChatRepository directChatRepository,
                                              MessageRepository messageRepository,
                                              SchedulersFacade schedulers) {
        return new ChatViewModel(messenger, peerRepository, directChatRepository, messageRepository, schedulers);
    }

    @Provides
    @Singleton
    static ChatListViewModel provideChatListViewModel(DirectChatRepository directChatRepository) {
        return new ChatListViewModel(directChatRepository);
    }

    /*
    @Provides
    @Singleton
    static OxSecretKeyBackupRestoreViewModel provideOxSecretKeyBackupRestoreViewModel(OpenPgpManager openPgpManager) {
        return new OxSecretKeyBackupRestoreViewModel(openPgpManager);
    }
     */

    /*
    @Provides
    @Singleton
    static ChatViewModel provideChatViewModel(Repositories repositories, SchedulersFacade schedulers) {
        return new ChatViewModel(repositories, schedulers);
    }
     */
}
