package org.mercury_im.messenger.core;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.CompletableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import lombok.Getter;

public class SchedulersFacade {

    /**
     * Name for the database / io thread scheduler.
     */
    public static final String SCHEDULER_IO = "DatabaseThread";

    /**
     * Name for the UI / main thread scheduler.
     */
    public static final String SCHEDULER_UI = "UIThread";

    /**
     * Name for a new thread scheduler.
     */
    public static final String SCHEDULER_NEW_THREAD = "NewThread";


    @Getter
    private final Scheduler ioScheduler;
    @Getter
    private final Scheduler uiScheduler;
    @Getter
    private final Scheduler newThread;

    @Inject
    public SchedulersFacade(@Named(value = SCHEDULER_IO) Scheduler ioScheduler,
                            @Named(value = SCHEDULER_UI) Scheduler uiScheduler,
                            @Named(value = SCHEDULER_NEW_THREAD) Scheduler newThread) {
        this.ioScheduler = ioScheduler;
        this.uiScheduler = uiScheduler;
        this.newThread = newThread;
    }

    public <A> ObservableTransformer<A, A> executeUiSafeObservable() {
        return upstream -> upstream.subscribeOn(ioScheduler).observeOn(uiScheduler);
    }

    public <A> SingleTransformer<A, A> executeUiSafeSingle() {
        return upstream -> upstream.subscribeOn(ioScheduler).observeOn(uiScheduler);
    }

    public <A> MaybeTransformer<A, A> executeUiSafeMaybe() {
        return upstream -> upstream.subscribeOn(ioScheduler).observeOn(uiScheduler);
    }

    public CompletableTransformer executeUiSafeCompletable() {
        return upstream -> upstream.subscribeOn(ioScheduler).observeOn(uiScheduler);
    }
}
