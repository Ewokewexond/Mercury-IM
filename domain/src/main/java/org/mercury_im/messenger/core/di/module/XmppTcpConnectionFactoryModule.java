package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.connection.XmppConnectionFactory;
import org.mercury_im.messenger.core.connection.XmppTcpConnectionFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class XmppTcpConnectionFactoryModule {

    @Provides
    @Singleton
    static XmppConnectionFactory provideConnectionFactory() {
        return new XmppTcpConnectionFactory();
    }
}
