package org.mercury_im.messenger.core.connection;

import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smackx.carbons.CarbonManager;
import org.jivesoftware.smackx.iqversion.VersionManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.sid.StableUniqueStanzaIdManager;

public class SmackConfig {

    static void staticConfiguration() {
        //SmackConfiguration.DEBUG = true;
        ReconnectionManager.setEnabledPerDefault(true);
        ReconnectionManager.setDefaultReconnectionPolicy(ReconnectionManager.ReconnectionPolicy.RANDOM_INCREASING_DELAY);

        VersionManager.setAutoAppendSmackVersion(false);
        VersionManager.setDefaultVersion("Mercury-IM", "0.0.1-little-joe");

        DeliveryReceiptManager.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.ifIsSubscribed);

        StableUniqueStanzaIdManager.setEnabledByDefault(true);

        CarbonManager.setEnabledByDefault(true);

        Roster.setRosterLoadedAtLoginDefault(true);
    }
}
