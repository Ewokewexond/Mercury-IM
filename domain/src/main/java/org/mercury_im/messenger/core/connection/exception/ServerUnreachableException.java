package org.mercury_im.messenger.core.connection.exception;

public class ServerUnreachableException extends Exception {

    private static final long serialVersionUID = 1L;

    public ServerUnreachableException(String message, Throwable cause) {
        super(message, cause);
    }
}
