package org.mercury_im.messenger.core.viewmodel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public interface MercuryViewModel {

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    default void dispose() {
        compositeDisposable.dispose();
    }

    default void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }
}
