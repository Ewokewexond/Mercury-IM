package org.mercury_im.messenger.core.exception;

import java.util.UUID;

import lombok.Getter;

public class ConnectionNotFoundException extends Exception {

    @Getter
    private final UUID accountId;

    public ConnectionNotFoundException(UUID accountId) {
        super("Connection with ID " + accountId.toString() + " not registered.");
        this.accountId = accountId;
    }
}
