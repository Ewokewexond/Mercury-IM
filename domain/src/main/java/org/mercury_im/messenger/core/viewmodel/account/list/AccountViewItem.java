package org.mercury_im.messenger.core.viewmodel.account.list;

import org.mercury_im.messenger.core.connection.state.ConnectivityState;
import org.mercury_im.messenger.entity.Account;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import lombok.Value;

@Value
public class AccountViewItem {

    Account account;
    ConnectivityState connectivityState;
    OpenPgpV4Fingerprint fingerprint;

}
