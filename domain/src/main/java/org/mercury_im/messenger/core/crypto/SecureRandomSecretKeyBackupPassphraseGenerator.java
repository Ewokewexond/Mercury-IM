package org.mercury_im.messenger.core.crypto;

import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;
import org.jivesoftware.smackx.ox.util.SecretKeyBackupHelper;

public class SecureRandomSecretKeyBackupPassphraseGenerator implements OpenPgpSecretKeyBackupPassphraseGenerator {

    @Override
    public OpenPgpSecretKeyBackupPassphrase generateBackupPassphrase() {
        return SecretKeyBackupHelper.generateBackupPassword();
    }

}
