package org.mercury_im.messenger.core.data.repository;

import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.chat.GroupChat;
import org.mercury_im.messenger.entity.message.Message;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface MessageRepository {

    Single<Message> insertMessage(DirectChat chat, Message message);

    Single<Message> insertMessage(GroupChat chat, Message message);

    Observable<List<Message>> observeMessages(DirectChat chat);

    Observable<List<Message>> observeMessages(GroupChat chat);

    Observable<List<Message>> findMessagesWithBody(String body);

    Observable<List<Message>> findMessagesWithBody(DirectChat chat, String body);

    Observable<List<Message>> findMessagesWithBody(GroupChat chat, String body);

    Single<Message> upsertMessage(DirectChat chat, Message message);

    Single<Message> upsertMessage(GroupChat chat, Message message);

    Single<Message> updateMessage(Message message);

    Completable deleteMessage(Message message);
}
