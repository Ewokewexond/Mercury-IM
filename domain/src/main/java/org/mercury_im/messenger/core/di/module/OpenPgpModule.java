package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.crypto.InsecureStaticSecretKeyBackupPassphraseGenerator;
import org.mercury_im.messenger.core.crypto.OpenPgpSecretKeyBackupPassphraseGenerator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class OpenPgpModule {

    @Singleton
    @Provides
    static OpenPgpSecretKeyBackupPassphraseGenerator provideSecretKeyBackupPassphraseGenerator() {
        // TODO: THIS MUST NEVER MAKE IT TO PRODUCTION!!!
        return new InsecureStaticSecretKeyBackupPassphraseGenerator();
    }

}
