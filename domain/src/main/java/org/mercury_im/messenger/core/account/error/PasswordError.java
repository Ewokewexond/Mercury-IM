package org.mercury_im.messenger.core.account.error;

public enum PasswordError {
    emptyPassword,
    incorrectPassword
}