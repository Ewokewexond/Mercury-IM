package org.mercury_im.messenger.data.mapping;

import org.junit.Test;
import org.mercury_im.messenger.data.di.component.DaggerMappingTestComponent;
import org.mercury_im.messenger.data.model.AccountModel;
import org.mercury_im.messenger.data.model.PeerModel;
import org.mercury_im.messenger.entity.contact.Peer;
import org.mercury_im.messenger.entity.contact.SubscriptionDirection;

import javax.inject.Inject;

import static junit.framework.TestCase.assertEquals;

public class PeerMappingTest {

    @Inject
    PeerMapping peerMapping;

    public static final Peer PEER_GORDO;

    static {
        PEER_GORDO = new Peer();
        PEER_GORDO.setAccount(AccountMappingTest.ACCOUNT_MISSION_CONTROL);
        PEER_GORDO.setAddress("gordo@big.joe");
        PEER_GORDO.setName("Gordo");
        PEER_GORDO.setSubscriptionDirection(SubscriptionDirection.both);
    }

    public PeerMappingTest() {
        DaggerMappingTestComponent.create().inject(this);
    }

    @Test
    public void entityToModel() {
        PeerModel model = peerMapping.toModel(PEER_GORDO);
        assertEquals(PEER_GORDO.getAddress(), model.getAddress());
        assertEquals(PEER_GORDO.getAccount().getAddress(), model.getAccount().getAddress());
        assertEquals(PEER_GORDO.getName(), model.getName());
    }

    @Test
    public void modelToEntity() {
        PeerModel model = new PeerModel();
        model.setName("Gordo");
        model.setAddress("gordo@big.joe");
        model.setAccount(new AccountModel());
    }
}
