package org.mercury_im.messenger.data.mapping;

import org.junit.Test;
import org.mercury_im.messenger.data.di.component.DaggerMappingTestComponent;
import org.mercury_im.messenger.data.model.EntityCapsModel;
import org.mercury_im.messenger.entity.caps.EntityCapsRecord;

import javax.inject.Inject;

import static junit.framework.TestCase.assertEquals;

public class EntityCapsMappingTest {

    @Inject
    EntityCapsMapping mapping;

    public EntityCapsMappingTest() {
        DaggerMappingTestComponent.create().inject(this);
    }

    @Test
    public void mapEntityToModelTest() {
        EntityCapsRecord entity = new EntityCapsRecord();
        entity.setNodeVer("thisisahash");
        entity.setXml("<xml/>");

        EntityCapsModel model = mapping.toModel(entity);

        assertEquals(entity.getNodeVer(), model.getNodeVer());
        assertEquals(entity.getXml(), model.getXml());
    }

    @Test
    public void mapModelToEntityTest() {
        EntityCapsModel model = new EntityCapsModel();
        model.setNodeVer("q07IKJEyjvHSyhy//CH0CxmKi8w=");
        model.setXml("<query xmlns='http://jabber.org/protocol/disco#info'" +
                "         node='http://psi-im.org#q07IKJEyjvHSyhy//CH0CxmKi8w='>" +
                "    <identity xml:lang='en' category='client' name='Psi 0.11' type='pc'/>" +
                "    <identity xml:lang='el' category='client' name='Ψ 0.11' type='pc'/>" +
                "    <feature var='http://jabber.org/protocol/caps'/>" +
                "    <feature var='http://jabber.org/protocol/disco#info'/>" +
                "    <feature var='http://jabber.org/protocol/disco#items'/>" +
                "    <feature var='http://jabber.org/protocol/muc'/>" +
                "    <x xmlns='jabber:x:data' type='result'>" +
                "      <field var='FORM_TYPE' type='hidden'>" +
                "        <value>urn:xmpp:dataforms:softwareinfo</value>" +
                "      </field>" +
                "      <field var='ip_version'>" +
                "        <value>ipv4</value>" +
                "        <value>ipv6</value>" +
                "      </field>" +
                "      <field var='os'>" +
                "        <value>Mac</value>" +
                "      </field>" +
                "      <field var='os_version'>" +
                "        <value>10.5.1</value>" +
                "      </field>" +
                "      <field var='software'>" +
                "        <value>Psi</value>" +
                "      </field>" +
                "      <field var='software_version'>" +
                "        <value>0.11</value>" +
                "      </field>" +
                "    </x>" +
                "  </query>");

        EntityCapsRecord entity = mapping.toEntity(model);

        assertEquals(model.getNodeVer(), entity.getNodeVer());
        assertEquals(model.getXml(), entity.getXml());
    }
}
