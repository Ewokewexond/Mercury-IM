package org.mercury_im.messenger.data.di.component;

import org.mercury_im.messenger.data.di.RepositoryModule;
import org.mercury_im.messenger.data.di.module.TestDatabaseModule;
import org.mercury_im.messenger.data.di.module.TestingSchedulerModule;
import org.mercury_im.messenger.data.repository.AccountRepositoryTest;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {
        RepositoryModule.class,
        TestDatabaseModule.class,
        TestingSchedulerModule.class
})
@Singleton
public interface InMemoryDatabaseComponent {

    void inject(AccountRepositoryTest test);
}
