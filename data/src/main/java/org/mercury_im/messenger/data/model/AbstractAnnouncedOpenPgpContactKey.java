package org.mercury_im.messenger.data.model;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.data.converter.EntityBareJidConverter;
import org.mercury_im.messenger.data.converter.OpenPgpV4FingerprintConverter;
import org.mercury_im.messenger.entity.Account;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.Date;
import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Key;
import io.requery.ReferentialAction;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "ox_announced_keys")
public class AbstractAnnouncedOpenPgpContactKey {

    @Key
    @Convert(UUIDConverter.class)
    @ForeignKey(references = AbstractAccountModel.class, delete = ReferentialAction.CASCADE)
    UUID accountId;

    @Key
    @Convert(EntityBareJidConverter.class)
    EntityBareJid owner;

    @Key
    @Convert(OpenPgpV4FingerprintConverter.class)
    OpenPgpV4Fingerprint fingerprint;

    @Column(name = "modification_date", nullable = false)
    Date modificationDate;
}
