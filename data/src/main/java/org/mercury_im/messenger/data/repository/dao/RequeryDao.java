package org.mercury_im.messenger.data.repository.dao;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

public abstract class RequeryDao {

    private final ReactiveEntityStore<Persistable> data;

    public RequeryDao(ReactiveEntityStore<Persistable> data) {
        this.data = data;
    }

    public ReactiveEntityStore<Persistable> data() {
        return data;
    }
}
