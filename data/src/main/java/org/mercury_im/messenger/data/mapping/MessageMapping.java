package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.MessageModel;
import org.mercury_im.messenger.entity.message.Message;


import javax.inject.Inject;

public class MessageMapping extends AbstractMapping<Message, MessageModel> {

    @Inject
    public MessageMapping() {

    }

    @Override
    public Message newEntity(MessageModel model) {
        return new Message();
    }

    @Override
    public MessageModel newModel(Message entity) {
        return new MessageModel();
    }

    @Override
    public MessageModel mapToModel(Message entity, MessageModel model) {
        model.setId(entity.getId());
        model.setSender(entity.getSender());
        model.setRecipient(entity.getRecipient());
        model.setTimestamp(entity.getTimestamp());
        model.setDirection(entity.getDirection());
        model.setBody(entity.getBody());
        model.setStanzaId(entity.getStanzaId());
        model.setOriginId(entity.getOriginId());
        model.setLegacyId(entity.getLegacyStanzaId());
        model.setXml(entity.getXml());

        return model;
    }

    @Override
    public Message mapToEntity(MessageModel model, Message entity) {
        entity.setId(model.getId());
        entity.setSender(model.getSender());
        entity.setRecipient(model.getRecipient());
        entity.setTimestamp(model.getTimestamp());
        entity.setDirection(model.getDirection());
        entity.setBody(model.getBody());
        entity.setStanzaId(model.getStanzaId());
        entity.setOriginId(model.getOriginId());
        entity.setLegacyStanzaId(model.getLegacyId());
        entity.setXml(model.getXml());
        return entity;
    }
}
