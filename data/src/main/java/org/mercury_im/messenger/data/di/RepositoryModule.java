package org.mercury_im.messenger.data.di;

import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.core.data.repository.DirectChatRepository;
import org.mercury_im.messenger.core.data.repository.EntityCapsRepository;
import org.mercury_im.messenger.core.data.repository.GroupChatRepository;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.core.data.repository.OpenPgpRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.data.mapping.AccountMapping;
import org.mercury_im.messenger.data.mapping.DirectChatMapping;
import org.mercury_im.messenger.data.mapping.EntityCapsMapping;
import org.mercury_im.messenger.data.mapping.GroupChatMapping;
import org.mercury_im.messenger.data.mapping.MessageMapping;
import org.mercury_im.messenger.data.mapping.PeerMapping;
import org.mercury_im.messenger.data.repository.RxAccountRepository;
import org.mercury_im.messenger.data.repository.RxDirectChatRepository;
import org.mercury_im.messenger.data.repository.RxEntityCapsRepository;
import org.mercury_im.messenger.data.repository.RxGroupChatRepository;
import org.mercury_im.messenger.data.repository.RxMessageRepository;
import org.mercury_im.messenger.data.repository.RxOpenPgpRepository;
import org.mercury_im.messenger.data.repository.RxPeerRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

@Module(includes = {
        MappingModule.class,
        DaoModule.class
})
public class RepositoryModule {

    @Provides
    @Singleton
    static AccountRepository provideAccountRepository(
            ReactiveEntityStore<Persistable> data,
            AccountMapping accountMapping) {
        return new RxAccountRepository(data, accountMapping);
    }

    @Provides
    @Singleton
    static PeerRepository providePeerRepository(
            ReactiveEntityStore<Persistable> data,
            PeerMapping peerMapping,
            AccountRepository accountRepository) {
        return new RxPeerRepository(data, peerMapping, accountRepository);
    }

    @Provides
    @Singleton
    static DirectChatRepository provideDirectChatRepository(
            ReactiveEntityStore<Persistable> data,
            DirectChatMapping directChatMapping) {
        return new RxDirectChatRepository(data, directChatMapping);
    }

    @Provides
    @Singleton
    static GroupChatRepository provideGroupChatRepository(
            ReactiveEntityStore<Persistable> data,
            GroupChatMapping groupChatMapping) {
        return new RxGroupChatRepository(data, groupChatMapping);
    }

    @Provides
    @Singleton
    static MessageRepository provideMessageRepository(
            ReactiveEntityStore<Persistable> data,
            MessageMapping messageMapping,
            DirectChatMapping directChatMapping,
            GroupChatMapping groupChatMapping) {
        return new RxMessageRepository(data, messageMapping, directChatMapping, groupChatMapping);
    }

    @Provides
    @Singleton
    static EntityCapsRepository provideCapsRepository(
            ReactiveEntityStore<Persistable> data,
            EntityCapsMapping entityCapsMapping) {
        return new RxEntityCapsRepository(data, entityCapsMapping);
    }

    @Provides
    @Singleton
    static OpenPgpRepository provideOpenPgpRepository(
            ReactiveEntityStore<Persistable> data, AccountRepository accountRepository) {
        return new RxOpenPgpRepository(data, accountRepository);
    }
}
