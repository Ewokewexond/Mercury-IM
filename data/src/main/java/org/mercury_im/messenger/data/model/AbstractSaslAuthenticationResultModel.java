package org.mercury_im.messenger.data.model;

import org.mercury_im.messenger.data.enums.SaslCondition;

import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "sasl_auth")
public abstract class AbstractSaslAuthenticationResultModel implements Persistable {

    @Key
    @ManyToOne
    AccountModel account;

    SaslCondition saslCondition;

    String descriptiveText;
}
