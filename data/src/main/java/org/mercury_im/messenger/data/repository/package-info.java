/**
 * The repository package contains implementations of the repositories defined in the domain
 * module.
 *
 * While the data module uses requery to store data in an SQL database, the repositories use
 * mappers defined in the mapping package to map the requery models to entities.
 *
 * Since the application itself only ever uses entities, it doesn't have to know about the database
 * at all.
 */
package org.mercury_im.messenger.data.repository;
