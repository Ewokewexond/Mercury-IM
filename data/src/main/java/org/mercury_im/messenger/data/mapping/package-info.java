/**
 * The mapping package contains mapper classes that map database models to entities.
 * The models can be found in {@link org.mercury_im.messenger.data.model} in this module,
 * while the entity classes are located in the <pre>entity</pre> module.
 *
 * The mapper classes define an architectural boundary that separates the entities from
 * their database model counterparts. This is done in order to keep the database logic separated
 * from the domain logic and to allow for quick replacement of the database implementation.
 *
 */
package org.mercury_im.messenger.data.mapping;
