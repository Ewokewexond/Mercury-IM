package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.core.util.Optional;

/**
 * Interface that defines a mapping between entities and database models.
 *
 * @param <E> Entity
 * @param <M> Model
 */
public interface Mapping<E, M> {

    /**
     * Map data from the entity to a new model.
     *
     * @param entity application entity
     * @return new database model
     */
    M toModel(E entity);

    Optional<M> toModel(Optional<E> entity);

    /**
     * Copy data from the model to a new entity.
     * @param model database model
     * @return new application entity
     */
    E toEntity(M model);

    Optional<E> toEntity(Optional<M> model);

    /**
     * Map an entity to a model.
     *
     * @param entity entity
     * @param model model
     * @return model
     */
    M toModel(E entity, M model);

    /**
     * Map a model to an entity.
     *
     * @param model model
     * @param entity entity
     * @return entity
     */
    E toEntity(M model, E entity);


}
