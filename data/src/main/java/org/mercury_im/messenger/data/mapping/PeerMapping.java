package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.PeerModel;
import org.mercury_im.messenger.entity.contact.Peer;

import javax.inject.Inject;

public class PeerMapping extends AbstractMapping<Peer, PeerModel> {

    private final AccountMapping accountMapping;

    @Inject
    public PeerMapping(AccountMapping accountMapping) {
        this.accountMapping = accountMapping;
    }

    @Override
    public Peer newEntity(PeerModel model) {
        return new Peer();
    }

    @Override
    public PeerModel newModel(Peer entity) {
        return new PeerModel();
    }

    @Override
    public PeerModel mapToModel(Peer entity, PeerModel model) {
        model.setId(entity.getId());
        model.setAccount(accountMapping.toModel(entity.getAccount(), model.getAccount()));
        model.setAddress(entity.getAddress());
        model.setName(entity.getName());

        model.setSubscriptionDirection(entity.getSubscriptionDirection());
        model.setSubscriptionPending(entity.isSubscriptionPending());
        model.setSubscriptionPreApproved(entity.isSubscriptionApproved());

        return model;
    }

    @Override
    public Peer mapToEntity(PeerModel model, Peer entity) {
        entity.setId(model.getId());
        entity.setAccount(accountMapping.toEntity(model.getAccount(), entity.getAccount()));
        entity.setAddress(model.getAddress());
        entity.setName(model.getName());

        entity.setSubscriptionDirection(model.getSubscriptionDirection());
        entity.setSubscriptionPending(model.isSubscriptionPending());
        entity.setSubscriptionApproved(model.isSubscriptionPreApproved());

        return entity;
    }
}
