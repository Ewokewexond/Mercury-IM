package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.GroupChatModel;
import org.mercury_im.messenger.entity.chat.GroupChat;

import javax.inject.Inject;

public class GroupChatMapping extends AbstractMapping<GroupChat, GroupChatModel> {

    private final AccountMapping accountMapping;

    @Inject
    public GroupChatMapping(AccountMapping accountMapping) {
        this.accountMapping = accountMapping;
    }

    @Override
    public GroupChat newEntity(GroupChatModel model) {
        return new GroupChat();
    }

    @Override
    public GroupChatModel newModel(GroupChat entity) {
        return new GroupChatModel();
    }

    @Override
    public GroupChatModel mapToModel(GroupChat entity, GroupChatModel model) {
        model.setAccount(accountMapping.toModel(entity.getAccount(), model.getAccount()));
        model.setAddress(entity.getRoomAddress());
        model.setName(entity.getRoomName());
        return model;
    }

    @Override
    public GroupChat mapToEntity(GroupChatModel model, GroupChat entity) {
        entity.setId(model.getId());
        entity.setAccount(accountMapping.toEntity(model.getAccount(), entity.getAccount()));
        entity.setRoomAddress(model.getAddress());
        entity.setRoomName(model.getName());
        return entity;
    }
}
