package org.mercury_im.messenger.data.model;

import org.mercury_im.messenger.data.converter.MessageDirectionConverter;
import org.mercury_im.messenger.entity.message.MessageDirection;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.Date;
import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "messages")
public abstract class AbstractMessageModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @Column(nullable = false)
    UUID chatId;

    @Column(nullable = false)
    String sender;

    @Column(nullable = false)
    String recipient;

    @Column(name = "\"timestamp\"", nullable = false)
    Date timestamp;

    @Column(nullable = false)
    @Convert(MessageDirectionConverter.class)
    MessageDirection direction;

    @Column(length = 65536)
    String body;

    @Column(length = 65536)
    String xml;

    @Column
    String legacyId;

    @Column(unique = true)
    String originId;

    @Column
    String stanzaId;

    @Column
    String stanzaIdBy;

    @Column
    OpenPgpV4Fingerprint senderOXFingerprint;
}
