package org.mercury_im.messenger.data.converter;

import org.mercury_im.messenger.data.enums.SaslCondition;

import io.requery.Converter;

public class SaslConditionConverter implements Converter<SaslCondition, String> {
    @Override
    public Class<SaslCondition> getMappedType() {
        return SaslCondition.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(SaslCondition value) {
        return value != null ? value.toString() : null;
    }

    @Override
    public SaslCondition convertToMapped(Class<? extends SaslCondition> type, String value) {
        return value != null ? SaslCondition.valueOf(value) : null;
    }
}
