package org.mercury_im.messenger.data.di;

import org.mercury_im.messenger.data.repository.dao.AccountDao;
import org.mercury_im.messenger.data.repository.dao.DirectChatDao;
import org.mercury_im.messenger.data.repository.dao.GroupChatDao;
import org.mercury_im.messenger.data.repository.dao.MessageDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

@Module
public class DaoModule {

    @Provides
    @Singleton
    static AccountDao provideAccountDao(ReactiveEntityStore<Persistable> data) {
        return new AccountDao(data);
    }

    @Provides
    @Singleton
    static DirectChatDao provideDirectChatDao(ReactiveEntityStore<Persistable> data) {
        return new DirectChatDao(data);
    }

    @Provides
    @Singleton
    static GroupChatDao provideGroupChatDao(ReactiveEntityStore<Persistable> data) {
        return new GroupChatDao(data);
    }

    @Provides
    @Singleton
    static MessageDao provideMessageDao(ReactiveEntityStore<Persistable> data) {
        return new MessageDao(data);
    }
}
