package org.mercury_im.messenger.data.model;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

@Table(name = "entity_caps")
@Entity
public abstract class AbstractEntityCapsModel implements Persistable {

    @Key
    String nodeVer;

    @Column(nullable = false, length = 65536)
    String xml;

}
