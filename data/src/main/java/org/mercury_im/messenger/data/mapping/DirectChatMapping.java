package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.DirectChatModel;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.contact.Peer;

import javax.inject.Inject;

public class DirectChatMapping extends AbstractMapping<DirectChat, DirectChatModel> {

    private final PeerMapping peerMapping;

    @Inject
    public DirectChatMapping(PeerMapping peerMapping) {
        this.peerMapping = peerMapping;
    }

    @Override
    public DirectChat newEntity(DirectChatModel model) {
        return new DirectChat();
    }

    @Override
    public DirectChatModel newModel(DirectChat entity) {
        return new DirectChatModel();
    }

    @Override
    public DirectChatModel mapToModel(DirectChat entity, DirectChatModel model) {
        model.setId(entity.getId());
        model.setPeer(peerMapping.toModel(entity.getPeer(), model.getPeer()));
        return model;
    }

    @Override
    public DirectChat mapToEntity(DirectChatModel model, DirectChat entity) {
        entity.setId(model.getId());
        Peer peer = peerMapping.toEntity(model.getPeer(), entity.getPeer());
        entity.setPeer(peer);
        entity.setAccount(peer.getAccount());
        return entity;
    }
}
