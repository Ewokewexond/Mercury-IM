package org.mercury_im.messenger.cli;

import java.util.Scanner;

public class ScannerUserInterface implements UserInterface {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String readText() {
        return scanner.nextLine();
    }

    @Override
    public String readPassword() {
        return scanner.nextLine();
    }
}
