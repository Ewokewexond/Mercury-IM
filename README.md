# Messenger

## Used Design Methods:

* Mercury IM's development follows architectural principles know from
[Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).
* The app is developed using the MVVM (Model View Viewmodel) pattern
* Components are wired together using Dependency Injection (DI) with [Dagger 2](https://dagger.dev)
* Data is persisted using the [requery](https://github.com/requery/requery) ORM framework

## Building

```
git clone <project-url>
cd <project-directory>
git submodule update --init --recursive
gradle assembleDebug
```

## FAQ

* I want to develop, but lots of `org.jivesoftware.smackx.*` classes cannot be found!
  * You forgot to type `git submodule init && git submodule update` as mentioned above
* It looks like I'm missing `org.mercury_im.messenger.data.*` classes???
  * In Android Studio select the `data` module and then click "Build -> Make Module 'data'".
