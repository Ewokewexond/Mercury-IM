# Mercury Feature Support

Mercury is compliant to [RFC 6120: Extensible Messaging and Presence Protocol (XMPP)](https://tools.ietf.org/html/rfc6120).


## XEP Support
Mercury currently has explicit support for the following XEPs:

* [XEP-0030: Service Discovery](https://xmpp.org/extensions/xep-0030.html)
* [XEP-0060: PubSub](https://xmpp.org/extensions/xep-0060.html)
* [XEP-0092: Software Version](https://xmpp.org/extensions/xep-0092.html)
* [XEP-0115: Entity Capabilities](https://xmpp.org/extensions/xep-0115.html)
* [XEP-0163: Personal Eventing Protocol](https://xmpp.org/extensions/xep-0163.html)
* [XEP-0198: Stream Management](https://xmpp.org/extensions/xep-0198.html)
* [XEP-0237: Roster Versioning](https://xmpp.org/extensions/xep-0237.html)
* [XEP-0280: Message Carbons](https://xmpp.org/extensions/xep-0280.html)
* [XEP-0352: Client State Indication](https://xmpp.org/extensions/xep-0352.html)
* [XEP-0359: Stable and Unique Stanza IDs](https://xmpp.org/extensions/xep-0359.html) see [comments](#comments)
* [XEP-0392: Consistent Color Generation](https://xmpp.org/extensions/xep-0392.html)


## Planned XEP Support
The following XEPs are on the TODO list:

* [XEP-0045: Multi User Chats](https://xmpp.org/extensions/xep-0045.html)
* [XEP-0048: Bookmark Storage](https://xmpp.org/extensions/xep-0048.html)
* [XEP-0054: vcard-temp](https://xmpp.org/extensions/xep-0054.html)
* [XEP-0077: Inband Registration](https://xmpp.org/extensions/xep-0077.html)
* [XEP-0080: User Location](https://xmpp.org/extensions/xep-0080.html) see [comments](#comments)
* [XEP-0084: User Avatars](https://xmpp.org/extensions/xep-0084.html) see [comments](#comments)
* [XEP-0085: Chat State Notifications](https://xmpp.org/extensions/xep-0085.html)
* [XEP-0184: Message Delivery Receipts](https://xmpp.org/extensions/xep-0184.html)
* [XEP-0192: Blocking Command](https://xmpp.org/extensions/xep-0192.html)
* [XEP-0249: Direct MUC Invitations](https://xmpp.org/extensions/xep-0249.html)
* [XEP-0308: Last Message Correction](https://xmpp.org/extensions/xep-0308.html)
* [XEP-0313: Message Archive Management](https://xmpp.org/extensions/xep-0313.html)
* [XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html)
* [XEP-0373](https://xmpp.org/extensions/xep-0373.html), [XEP-0374: OpenPGP for XMPP](https://xmpp.org/extensions/xep-0374.html)
* [XEP-0384: OMEMO Encryption](https://xmpp.org/extensions/xep-0384.html)
* [XEP-0394: Message Markup](https://xmpp.org/extensions/xep-0394.html)
* [XEP-0410: MUC Self-Ping](https://xmpp.org/extensions/xep-0410.html)
* [XEP-0420: Stanza Content Encryption](https://xmpp.org/extensions/xep-0420.html)


## Not Planned XEP Support
The following XEPs are *not* currently on the TODO list:

* [XEP-0071: XHTML-IM](https://xmpp.org/extensions/xep-0071.html) see [comments](#comments)
* [XEP-0357: Push Notifications](https://xmpp.org/extensions/xep-0357.html) see [comments](#comments)


## Comments

* XEP-0071: XEP is deprecated and considered a potential security risk
* XEP-0080: Presumably in form of an addon?
* XEP-0084: Work is currently done [upstream in Smack](https://github.com/igniterealtime/Smack/pull/332)
* XEP-0357: Mercury embraces decentralization and independence. This is not compatible with being functionally dependent on centralized push services.
* XEP-0359: Currently broken upstream, but a [fix](https://github.com/igniterealtime/Smack/pull/344) is submitted
