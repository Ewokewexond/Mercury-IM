package org.mercury_im.messenger.entity.chat;

import org.mercury_im.messenger.entity.contact.Peer;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Interface that describes a direct chat between the user and another one.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DirectChat extends Chat {
    Peer peer;

    public DirectChat() {
        super();
    }
}
