package org.mercury_im.messenger.entity.message.content;

import lombok.Data;

@Data
public class TextPayload implements Payload.Body {
    long id;
    String body;
}
