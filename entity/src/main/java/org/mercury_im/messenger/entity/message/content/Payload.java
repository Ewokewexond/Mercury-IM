package org.mercury_im.messenger.entity.message.content;

/**
 * Interface describing contents of a message.
 *
 * A message consists of general information like sender, recipient etc.
 * Additionally a message contains at least one payload container, eg. the set of unencrypted
 * message contents, or the set of encrypted contents.
 *
 * Inside those payloads, message contents exist.
 */
public interface Payload {

    long getId();

    void setId(long id);

    interface Body extends Payload {

        String getBody();

        void setBody(String body);

    }
}
