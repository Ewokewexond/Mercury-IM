package org.mercury_im.messenger.entity;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;

import java.util.UUID;

import lombok.Data;

/**
 * User Account entity.
 */
@Data
public class Account {
    UUID id;
    String address;
    String password;
    String host;
    int port;
    boolean enabled;
    String rosterVersion;

    public Account() {
        this.id = UUID.randomUUID();
        this.rosterVersion = "";
    }

    public EntityBareJid getJid() {
        return JidCreate.entityBareFromOrThrowUnchecked(getAddress());
    }
}
