package org.mercury_im.messenger.entity.chat;

import lombok.Data;

/**
 * Interface that describes typical preferences in the context of a chat.
 */
@Data
public class ChatPreferences {
    NotificationPreferences notificationPreferences;
    boolean typingNotificationsSupported;
    boolean sendTypingNotificationsEnabled;
    boolean readNotificationsSupported;
    boolean sendReadNotificationsEnabled;

    @Data
    public static class NotificationPreferences {
        boolean notifyOnMessage;
        boolean notifyOnMention;
    }
}
