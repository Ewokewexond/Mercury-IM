package org.mercury_im.messenger.entity.chat;

import org.mercury_im.messenger.entity.contact.Peer;

import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * An interface that describes a group chat entity.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GroupChat extends Chat {
    Set<Peer> participants;
    String roomAddress;
    String roomName;
}
