package org.mercury_im.messenger.entity.message;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class Message {
    UUID id;
    String sender;
    String recipient;
    Date timestamp;
    MessageDirection direction;
    String body;
    MessageDeliveryState deliveryState;
    String legacyStanzaId;
    String originId;
    String stanzaId;
    String xml;
    boolean encrypted;
    boolean received;
    boolean read;

    public boolean isIncoming() {
        return getDirection() == MessageDirection.incoming;
    }

    public Message() {
        this.id = UUID.randomUUID();
    }
}
