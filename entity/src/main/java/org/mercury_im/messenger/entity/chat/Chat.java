package org.mercury_im.messenger.entity.chat;

import org.mercury_im.messenger.entity.Account;

import java.util.UUID;

import lombok.Data;

/**
 * Generic interface defining shared properties of chats.
 *
 * Child interfaces of {@link Chat} are {@link DirectChat} and {@link GroupChat}.
 */
@Data
public abstract class Chat {
    UUID id;
    Account account;
    ChatPreferences chatPreferences;

    public Chat() {
        this.id = UUID.randomUUID();
    }
}
